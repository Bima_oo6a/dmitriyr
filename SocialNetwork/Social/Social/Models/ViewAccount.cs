﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Social.Models
{
    public class ViewAccount
    {
        private Account account;
    }
    public class RegistrationAccount 
    {
        [Required]
        [Display(Name = "Имя:")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Фамилия:")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Отчество:")]
        public string MiddleName { get; set; }
        
        [Display(Name = "День рождения:")]
        public string Birthday { get; set; }

        [Display(Name = "Номер телефона:")]
        public string NumberPhone { get; set; }



        [Required]
        [Display(Name = "Логин:")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}