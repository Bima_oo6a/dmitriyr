﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Social.Models
{
    public class MyUser
    {
        public virtual MyUserInfo MyUserInfo { get; set;}
    }

    public class MyUserInfo {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Birthday { get; set; }
        public MyUserPhone UserPhone { get; set; }

    }
    public class MyUserPhone 
    {
        public int Id { get; set; }
        public string NPhone { get; set; }
    }
    
}